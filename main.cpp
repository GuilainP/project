#include <iostream>
#include <wiringPi.h>
#include <wiringSerial.h>
#include <chrono>
#include <thread>
#include <fstream>

#define MOTION_DETECTED 0
#define MOTION_SENSOR   20

#define MOTION_DURATION 12000
#define SOUND_DURATION  800

void save_data(const std::string& detectionType, const int& duration){
	
    time_t current_time;
    struct tm* ti;
    time(&current_time);
    ti = localtime(&current_time);

    std::string month, day, hour, minute, sec;
    ti->tm_mon < 10 ? month = "0" + std::to_string(ti->tm_mon + 1) : month = std::to_string(ti->tm_mon +1);
    ti->tm_mday < 10 ? day = "0" + std::to_string(ti->tm_mday) : day = std::to_string(ti->tm_mday);
    ti->tm_hour < 10 ? hour = "0" + std::to_string(ti->tm_hour) : hour = std::to_string(ti->tm_hour);
    ti->tm_min < 10 ? minute = "0" + std::to_string(ti->tm_min) : minute = std::to_string(ti->tm_min);
    ti->tm_sec < 10 ? sec = "0" + std::to_string(ti->tm_sec) : sec = std::to_string(ti->tm_sec);

    // YYYY_MM_DD-hh:mm:ss
    auto date = std::to_string(ti->tm_year+1900) + '_' + 
                                month + '_' + 
                                day + '-' + 
                                hour + ':' + 
                                minute + ':' +
                                sec ;

	std::ofstream fichier;
	fichier.open("log.txt", std::ios_base::app);
	fichier << date << " | " << detectionType << " | " << duration << "ms\n";	
}
/** 
 * port GPIO 14 (n°8)  : Rx => ttyS0
 * port GPIO 15 (n°10) : Tx => ttyS0
 */
int main() { 
    int fdX;
    char val;
	int counter = 1;
	// open ports Tx and Rx
	fdX = serialOpen("/dev/ttyS0", 9600);
	if(fdX == -1) {
		std::cout << "Unable to open serial port\n";
		return -1;
	}

	wiringPiSetupGpio();
	pinMode(MOTION_SENSOR, INPUT);

    while(1) {

		val = '0';
		int nb = digitalRead(MOTION_SENSOR);
		if (nb == MOTION_DETECTED) {
			std::cout << "Motion detected" << std::endl;
			serialFlush(fdX);
			serialPutchar(fdX, 'm');
			save_data("motion", MOTION_DURATION);
			counter++;
			std::this_thread::sleep_for(std::chrono::milliseconds(MOTION_DURATION));
		} else if(serialDataAvail(fdX) != -1) {
			val = serialGetchar(fdX);
			if(val == 's'){
				std::cout << "Sound detected" << std::endl;
				save_data("sound", SOUND_DURATION);
				counter++;
			}
			std::this_thread::sleep_for(std::chrono::milliseconds(SOUND_DURATION));
		}
		
		if(counter > 9){
			std::cout << "------------------------\n";
			std::system("git add log.txt");
			std::system("git commit -m \"new log\"");
			std::system("git push origin main");
			std::cout << "------------------------\n";
			counter = 1;
		}
    }
}
