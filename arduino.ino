#define SOUND_SENSOR 5
#define LED_GREEN_1 7
#define LED_GREEN_2 8
#define LED_YELLOW_1 9
#define LED_YELLOW_2 10
#define LED_RED_1 11
#define LED_RED_2 12

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_GREEN_1, OUTPUT);
  pinMode(LED_GREEN_2, OUTPUT);
  pinMode(LED_YELLOW_1, OUTPUT);
  pinMode(LED_YELLOW_2, OUTPUT);
  pinMode(LED_RED_1, OUTPUT);
  pinMode(LED_RED_2, OUTPUT);

  // Broche en entrée
  pinMode(SOUND_SENSOR, INPUT);
  
  Serial.begin(9600);  // Ouvre le port série à 9600 bauds
}

void blink_led(int led1, int led2){
  digitalWrite(led1,HIGH);
  digitalWrite(led2,HIGH);
  delay(200);
  digitalWrite(led1,LOW);
  digitalWrite(led2,LOW);
  delay(200);
}

void chenillards()
{
  for(int i=LED_GREEN_1; i<= LED_RED_2; i++){
    digitalWrite(i,HIGH);
    delay(100);
  }
  for(int i=LED_GREEN_1; i<= LED_RED_2; i++){
    digitalWrite(i,LOW);
    delay(100);
  }
}

void motion_detected() {
  // pendant 5 secondes
  for(int i = 0 ; i < 5 ; i++) {
    digitalWrite(LED_YELLOW_1, HIGH);
    digitalWrite(LED_YELLOW_2, HIGH);
    delay(500);
    digitalWrite(LED_YELLOW_1, LOW);
    digitalWrite(LED_YELLOW_2, LOW);
    delay(500);
  }

  // pendant 2 secondes
  for(int i = 0 ; i < 5 ; i++) {
    digitalWrite(LED_GREEN_1, HIGH);
    digitalWrite(LED_GREEN_2, HIGH);
    digitalWrite(LED_RED_1, HIGH);
    digitalWrite(LED_RED_2, HIGH);
    delay(200);
    digitalWrite(LED_RED_1, LOW);
    digitalWrite(LED_RED_2, LOW);
    digitalWrite(LED_GREEN_1, LOW);
    digitalWrite(LED_GREEN_2, LOW);
    delay(200);
  }

  for(int i = 0 ; i < 5 ; i++) {
    digitalWrite(LED_YELLOW_1, HIGH);
    digitalWrite(LED_YELLOW_2, HIGH);
    delay(500);
    digitalWrite(LED_YELLOW_1, LOW);
    digitalWrite(LED_YELLOW_2, LOW);
    delay(500);
  }
}


void loop() {
   // put your main code here, to run repeatedly:
   //Serial.println(analogRead(SOUND_SENSOR));
   // Rx
   if(Serial.available()) {
      if(Serial.read() == 'm'){
        motion_detected();
        Serial.flush();
      }
   } else if( analogRead(SOUND_SENSOR) > 285 ){  // seuil
     blink_led(LED_GREEN_1, LED_GREEN_2);
     blink_led(LED_RED_1, LED_RED_2);
     Serial.write('s');
   } else {
     chenillards();
   }

   //delay(1000);
}

