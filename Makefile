
CC=g++
STD=c++17
CFLAGS=-Wall

main:main.o
	$(CC) -o main main.o -lwiringPi

main.o:main.cpp
	$(CC) -std=$(STD) $(FLAGS) -c main.cpp
clean:
	rm main main.o
